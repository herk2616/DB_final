---
title: "Databases final excersize"
author: "Irakleios Kopitas, Bob kocour"
date: "07 Jun 2020"
geometry: margin=1cm
output: pdf_document
header-includes:
- \setlength{\parindent}{2em}
- \setlength{\parskip}{0em}
---
![Unipi](http://www.unipi.gr/unipi/odigos_spoudon/unipilogo.png)


\pagebreak

# Λογικό διάγραμμα

![logical](./db/Logical.pdf)

\pagebreak

# Σχεσιακό διάγραμμα

![relational](./db/Relational.pdf)


\pagebreak

# Παραδοχές

* Η κάθε χώρα έχει σχέση ένα προς ένα με μία αερογραμμή.

\

* Από αυτές η κάθε αερογραμμή έχει σχέση ένα προς πολλά με τα γραφεία κρατήσεων.

\

* Το κάθε γραφείο κρατήσεων κανονίζει πολλές πτήσεις, οπότε έχει σχέση ένα προς
πολλά με τις πτήσεις.

\

* Μία πτήση μπορεί να έχει πολλά εισιτήρια, δηλαδή έχει σχέση ένα προς πολλά με
τα εισιτήρια.

\

* Και τέλος ο κάθε πελάτης μπορεί να έχει πολλά εισιτήρια, οπότε είναι άλλη μια
σχέση ένα προς πολλά για τα εισιτήρια.

\
\


# Η Εφαρμογή
Τρέχοντας το πρόγραμμα βλέπουμε την αρχική οθόνη με τις παρακάτω επιλογές.


```awk
   __,   .  ,_  ,_    _,_ ,_  -/-     __   //  .  _   ,__,  -/-
  (_/(__/__/ (__/__ +(_/_/ (__/_    _(_,__(/__/__(/__/ / (__/_
               /
              /

     1. To insert new flight
     2. To insert new customer
     3. To insert new ticket
     4. To return flights
     5. To return full flights
     6. Return flights from Toronto to New york
     7. Delete canseled tickets
     8. Payment portal

     0. Exit


[ console ~] :
```


\pagebreak

## 1. To insert new flight

Επιλέγοντας 1. η εφαρμογή θα ξεκινήσει την διαδικασία δημιουργίας πτήσης.
Θα εμφανιστεί το ακόλουθο μενού.

\

Πληκτρολογούμε την πόλη του εκδοτικού παρατήματος που έχει η κάθε αερογραμμή.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```
[Montreal, Toronto, Nikaia, Paris, Emdiburg,
        London, Napole, Rome, Berlin, Bonne, New york, Sicago]

Enter office location from above  (Enter exit to quit)> Rome
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\
\

Έπειτα πληκτρολογούμε την πόλη προορισμού της πτήσης.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```
Enter arrival city name > New york
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\
\

Στην συνέχεια συμπληρώνουμε την ημερομηνία και ώρα άφιξης.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```
Enter arrival date and time

>Enter date (year)    : 2020
>Enter date (month)   : 09
>Enter date (day)     : 22
>Enter time (hour)    : 16
>Enter time (minutes) : 30
>Enter time (sec)     : 00
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\
\

Μετά γράφουμε την ημερομηνία και ώρα αναχώρησης.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```
Enter departure date and time

>Enter date (year)    : 2020
>Enter date (month)   : 09
>Enter date (day)     : 22
>Enter time (hour)    : 10
>Enter time (minutes) : 45
>Enter time (sec)     : 00
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\
\

Ύστερα συμπληρώνουμε το μοναδικό χαρακτηριστικό της πτήσης.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```
Enter unique key identifier > fl4
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\
\

Και τέλος τις θέσεις, επαγγελματικές και μη, την δυνατότητα καπνίσματος και
την τιμή της πτήσης.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```
Enter total bussines seats > 30
Enter total economy seats > 70
Smoking? [1/0] > 0
Enter Flight price > 50
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}
\pagebreak

## 2. To insert new customer
Επιλέγοντας 2. η εφαρμογή θα ξεκινήσει την διαδικασία εγγραφής νέου πελάτη.
Θα εμφανιστεί το ακόλουθο μενού.

\

Αρχικά προσθέτουμε το όνομα του πελάτη.

\begin{center}\rule{4.8in}{0.4pt}\end{center}
```
Enter Customers Name     > Jack
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\
\

Έπειτα τοποθετούμε το επώνυμο του.

\begin{center}\rule{4.8in}{0.4pt}\end{center}
```
Enter Customers Surname  > Brown
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\
\

Στην συνέχεια πληκτρολογούμε την χώρα προέλευσής του.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```
Enter Customers Country  > Sweeden
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\
\

Μετά γράφουμε την διεύθυνση του.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```
Enter Customers Address  > 3 St.reih, Rainsvire
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\
\

Έπειτα το τηλέφωνο και το φάξ του.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```
Enter Customers PhoneNum > +021-237-4273, +021-745-1731
Enter Customers FaxNum   > +021-237-4273
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\
\

Στην συνέχεια την διεύθυνση ηλεκτρονικού ταχυδρομείου του.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```
Enter Customers Email    > JackBrown@googleplexmail.sw
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\
\

Και τέλος το μοναδικό χαρακτηριστικό του.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```
Enter Customers id       > hm6

```
\begin{center}\rule{4.8in}{0.4pt}\end{center}
\pagebreak

## 3. To insert new ticket
Η τρίτη επιλογή δημιουργεί ένα εισιτήριο. Προαπαιτείτε να υπάρχουν διαθέσιμες
πτήσεις και πελάτες, διαφορετικά η δημιουργία του εισιτηρίου θα αποτύχει.

\

Αρχικά απαντάμε αν η θέση είναι επαγγελματική η όχι.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```
Is the seat in bussines class? [1/0] > 1
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\

Έπειτα επιλέγουμε μία πτήση από την λίστα. Αν δεν υπάρχουν πτήσεις τότε μπορούμε
να γυρίσουμε στο προηγούμενο μενού γράφοντας ` exit `.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```
Select customers id from above (Enter exit to quit) > hm1
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\
\

Στην συνέχεια πληκτρολογούμε την πόλη του εκδοτικού παρατήματος που εκδίδεται το
εισιτήριο.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```
[Montreal, Toronto, Nikaia, Paris, Emdiburg,
        London, Napole, Rome, Berlin, Bonne, New york, Sicago]

Enter office location from above  (Enter exit to quit) > Rome
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\
\

Και τέλος εισάγουμε το μοναδικό χαρακτηριστικό του εισιτηρίου.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```
Enter ticket id > tk8
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}
\pagebreak

## 4. To return flights
Η επιλογή αυτή εμφανίζει όλες τις διαθέσιμες πτήσεις.

## 5. To return full flights
Όπως η επάνω επιλογή εμφανίζει πτήσεις αλλά με την διαφορά ότι εκτυπώνει τις
πτήσεις πού δεν έχουν επαγγελματικές και μη θέσεις.

## 6. Return flights from Toronto to New york
Αυτή η επιλογή εκτυπώνει πτήσεις από το Τορόντο στη Νέα Υόρκη.

## 7. Delete canseled tickets
Με αυτή την επιλογή όλες οι πτήσεις μαρκαρισμένες σαν ακυρωμένες διαγράφονται.
Υπάρχει η ίδια μέθοδος στο μενού εξόφλησης για ευκολία.

\pagebreak

# 8. Payment portal
Αυτό είναι το μενού εξόφλησης.

```
 _ __                                  _ __                 _
' )  )                           _/_  ' )  )      _/_      //
 /--'__.  __  , ______  _  ____  /     /--'__ __  /  __.  //
/   (_/|_/ (_/_/ / / <_</_/ / <_<__   /   (_)/ (_<__(_/|_</_
            /
           '

     1. To check your ticket - tikets
     2. To pay for a ticket
     3. To cancel a ticket
     4. Delete canseled tickets

     0. Exit


[ console ~] :

```

## 1. To check your ticket - tickets
Η πρώτη επιλογή δίνει την δυνατότητα επισκόπησης των πεδίων του εισιτηρίου ενώς
χρήστη.

\

Αρχικά ζητάει την επιλογή ενός πελάτη από την λίστα.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```
Select customer id from above  (Enter exit to quit)> hm1
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\
\


Έπειτα επιλέγουμε ένα εισιτήριο τού χρήστη από την λίστα.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```
Select ticket from above  (Enter exit to quit)> tk1
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\
\

Και τέλος δίνει όλες τις πληροφορίες που θέλουμε για ένα εισιτήριο.
\begin{center}\rule{4.8in}{0.4pt}\end{center}

```
arrivalDateTime   > 2020-02-02 02:03:02.0
cityName   > Toronto
departureDateTime   > 2020-02-02 02:02:02.0
flightPrice   > 200
flightPriceLocal   > 306
priceDeposited   > 0
priceRemaining   > 201
ticketBussinessFlag   > 0
ticketDate   > 2020-06-07 02:44:05.0
ticketId   > tk1
ticketStatus   > c
totalPrice   > 201
totalPriceLocal   > 307.53000000000003
Flight_flightId   > fl1
Customer_customerId   > hm1

```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\pagebreak

## 2. To pay for a ticket
Αυτή η επιλογή αφήνει τον χρήστη να πληρώσει για το εισιτήριο του.

\

Ανοίγοντας την μας ζητάει την επιλογή του πελάτη.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```
Select customer id from above  (Enter exit to quit)> hm1
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\
\


Έπειτα επιλέγουμε ένα εισιτήριο τού χρήστη από την λίστα.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```
Select ticket from above  (Enter exit to quit)> tk1
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\
\

Στην συνέχεια ζητάει από τον χρήστη να εισάγει ένα ποσό από το κόστος του εισιτηρίου.
Καταθέτοντας οπουδήποτε μικρότεροι η ίσο ποσό επιστρέφουμε στο μενού. Διαφορετικά
αναζητάει το ποσό.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```
Enter money to deposit. (remaining : 201.0 )
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\pagebreak


# Code

Ο κώδικας της εφαρμογής είναι χωρισμένος σε 5 κλάσεις
{ Client Customer Flight PaymentPortal Ticket }.

## Client

Η Client είναι ο συνδετικός κρίκος τού προγράμματος αφού έχει την main
και βασικές μεθόδους εισαγωγής του χρήστη που χρησιμοποιούνται σε όλες
τις κλάσεις.
Επίσης δημιουργεί την σύνδεση με την βάση δεδομένων και αρχικοποιεί
τις τιμές των χωρών, και αερογραμμών.

\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Java
class Client{
	public static void main(String[] args){

		/*
		 * Sql Connection
		 */

		Connection conn = null;
		String url = "jdbc:mysql://192.168.2.113:3306/";
		String dbName = "airport_base";
		String driver = "com.mysql.jdbc.Driver";
		String userName = "herk";
		String password = "2616";

		/*
		 * Sql Driver
		 */

		try{
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url+dbName,userName,password);
			System.out.println("Connected to database");
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("Connection failed");
		}

		try{
			Statement stat = conn.createStatement();
			stat.execute("SET FOREIGN_KEY_CHECKS = 0 ");

			// Drop all tables
			stat.executeUpdate("DELETE FROM country WHERE 1");
			stat.executeUpdate("DELETE FROM airline WHERE 1");
			stat.executeUpdate("DELETE FROM booking WHERE 1");
			stat.executeUpdate("DELETE FROM flight WHERE 1");
			stat.executeUpdate("DELETE FROM ticket WHERE 1");
			stat.executeUpdate("DELETE FROM customer WHERE 1");

			// Countys
			stat.executeUpdate("INSERT INTO country VALUES('Canada', 'CAD', 1.53, 1)");
			stat.executeUpdate("INSERT INTO country VALUES('Usa', 'USD', 1.11, 1)");
			stat.executeUpdate("INSERT INTO country VALUES('Italy', 'ITL', 2000, 1500)");
			stat.executeUpdate("INSERT INTO country VALUES('Britain', 'GBP', 0.90, 1)");
			stat.executeUpdate("INSERT INTO country VALUES('France', 'FRF', 5.89, 5)");
			stat.executeUpdate("INSERT INTO country VALUES('Germany', 'DEM', 1.76, 2)");

			// Airline
			stat.executeUpdate("INSERT INTO airline VALUES('AirCan', 'Canada')");
			stat.executeUpdate("INSERT INTO airline VALUES('USAir', 'Usa')");
			stat.executeUpdate("INSERT INTO airline VALUES('ItalAir', 'Italy')");
			stat.executeUpdate("INSERT INTO airline VALUES('BritAir', 'Britain')");
			stat.executeUpdate("INSERT INTO airline VALUES('AirFrance', 'France')");
			stat.executeUpdate("INSERT INTO airline VALUES('LuftAir', 'Germany')");


			/*
			 * Main loop
			 */

			int inp         = 0;
			int exitTrigger = 1;
			while ( exitTrigger == 1 ) {
System.out.println(
"                                            _                   \n" +
"   __,   .  ,_  ,_    _,_ ,_  -/-     __   //  .  _   ,__,  -/- \n" +
"  (_/(__/__/ (__/__ +(_/_/ (__/_    _(_,__(/__/__(/__/ / (__/_  \n" +
"               /                                                \n" +
"              /                                                 \n" +
"                                                                \n" +
"     1. To insert new flight                                    \n" +
"     2. To insert new customer                                  \n" +
"     3. To insert new ticket                                    \n" +
"     4. To return flights                                       \n" +
"     5. To return full flights                                  \n" +
"     6. Return flights from Toronto to New york                 \n" +
"     7. Delete canceled tickets                                 \n" +
"     8. Payment portal                                          \n" +
"                                                                \n" +
"     0. Exit                                                    \n" +
"                                                                \n");
				Scanner scan = new Scanner(System.in);
				inp = isMenuOption();

				/*
				 * Selection
				 */

				switch (inp){
					case 0: // Opt 0 Exit
						clearScreen();
						exitTrigger = 0;
						break;
					case 1: // Opt1 new Flight
						try{
							Flight fl = new Flight(stat);
							stat.executeUpdate(fl.returnDbData());
						}catch(Exception e){
							System.out.println("ERROR : Flight generation \n {"+
									e.getMessage()+ "}\n");//<-
						}
						clearScreen();
						break;
					case 2: // Opt2 New customer
						try{
							Customer cs = new Customer();
							stat.executeUpdate(cs.returnDbData());
						}catch(Exception e){
							System.out.println("ERROR : Customer generation \n {"+
									e.getMessage()+ "}\n");//<-
						}
						clearScreen();
						break;
					case 3: // Opt3 New Ticket
						try{
							Ticket tk = new Ticket(stat);
							stat.executeUpdate(tk.returnDbData());
						}catch(Exception e){
							System.out.println("ERROR : Ticket generation \n {"+
									e.getMessage()+ "}\n");//<-
						}
						clearScreen();
						break;
					case 4: // Opt4 return flights
						Flight.checkFlights(stat);
						break;
					case 5: // Opt5 return full flights
						Flight.checkFullFlights(stat);
						break;
					case 6: // Opt6
						try{
							ResultSet rs = stat.executeQuery("SELECT * FROM `flight` WHERE `departureCity`" +
							" = 'Toronto' AND `arrivalCity` = 'New york' ORDER BY `flight`.`flightId` ASC");
							while(rs.next())
								System.out.println("Flights -> " + rs.getString("flightId") + "\n" );//<-
						}catch(Exception e){
							System.out.println("exception");//<-
						}
						break;
					case 7: // Opt7
						clearScreen();
						PaymentPortal.deleteCanceledTickets(stat);
						break;
					case 8: // Opt8
						clearScreen();
						PaymentPortal.main();
						clearScreen();
						break;
				} 	// End of switch
			} 	// End of while

		}catch(Exception e){
			System.out.println("ERROR: Client.main" +
					e.getMessage() );

		} 	// End of Trycatch
	}	// End of main


	/*
	 * Basic functions
	 */


	public static void clearScreen() {
		for (int i = 0; i < 50; ++i) { //clear console simulation for Eclipse
			System.out.println();
		}
		System.out.print("\033\143\n"); //for terminal
	}


	public static void pressEnter(){
		Scanner sc = new Scanner(System.in);
		System.out.println("Press Enter to continue...");
		sc.nextLine();
	}


	public static int isMenuOption() {
		Scanner sc = new Scanner(System.in);
		int number;
		do {
			System.out.print("[ console ~] : ");
			while (!sc.hasNextInt()) {
				System.out.println(">Not an integer");
				System.out.print("[ console ~] : ");
				sc.next();
			}
			number = sc.nextInt();
		} while (number < 0 || number > 8);
		return number;
	}


	/*
	 * Input functions
	 */


	public static String selectFromCountry(Statement stat){
		ArrayList<String> Countries = new ArrayList<>();
		try{
			ResultSet rs = stat.executeQuery("SELECT * FROM `country`");
			while (rs.next())
				Countries.add(rs.getString("countryName"));
		}catch (Exception e){
			System.err.println("exception in selectFromCountry\n" +
					e.getMessage() + "\n");
		}
		String input;
		do{
			System.out.println("\n" + Countries + "\n");
			input = Client.askString("Select country from above (Enter exit to quit)> ");
			if(input.equals("exit")){
				return null;
			}
		}while (!Countries.contains(input));
		return input;
	}


	public static String askString(String message){
		Scanner sc = new Scanner(System.in);
		System.out.print(message);
		String string;
		string = sc.nextLine();
		System.out.println("{" + string + "} Inserted");
		return string;
	}


	public static int askInteger(String message) {
		Scanner sc = new Scanner(System.in);
		System.out.print(message);
		int number;
		while (!sc.hasNextInt()) {
			System.out.println(">Not an integer");
			System.out.print(message);
			sc.next();
		}
		number = sc.nextInt();
		return number;
	}


	public static boolean askBoolean(String message){
		Scanner sc = new Scanner(System.in);
		System.out.print(message);
		boolean bool;
		while (!sc.hasNextBoolean()) {
			System.out.println(">Not a Boolean value");
			System.out.print(message);
			sc.next();
		}
		bool = sc.nextBoolean();
		return bool;
	}


	public static double askDouble(String message){
		Scanner sc = new Scanner(System.in);
		System.out.print(message);
		double doub;
		while (!sc.hasNextDouble()) {
			System.out.println(">Not a Double value");
			System.out.print(message);
			sc.next();
		}
		doub = sc.nextDouble();
		return doub;
	}


	public static LocalDateTime askDateTime(String message){
		System.out.println(message + "\n");//<-
		int year, month, day, hour, min, sec;
		do {
			year = askInteger("\n>Enter date (year) : ");
		} while (year < 1880 || year > 2090 );
		do {
			month = askInteger("\n>Enter date (month) : ");
		} while (month < 1 || month > 12 );
		do {
			day = askInteger("\n>Enter date (day) : ");
		} while (day < 1 || day > 31);
		do {
			hour = askInteger("\n>Enter time (hour) : ");
		} while (hour < 0 || hour > 23);
		do {
			min = askInteger("\n>Enter time (minutes) : ");
		} while (min < 0 || min > 59);
		do {
			sec = askInteger("\n>Enter time (sec) : ");
		} while (sec < 0 || sec > 59);
		LocalDateTime datetime = LocalDateTime.of(year, month, day, hour, min, sec);
		System.out.println("{" + datetime + "} Inserted");
		return datetime;
	}   // End of checkFlightDate


}	// End of class
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}
\pagebreak


## Customer

Η Customer δημιουργεί ένα αντικείμενο χρήστη που με τον κατασκευαστή του
αρχικοποιεί τιμές και με την μέθοδο returnDbData που επιστρέφει τα δεδομένα
σε μορφή εισαγωγής για την sql είναι  έτοιμα να μπουν στην βάση δεδομένων.



\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Java
class Customer{
	private String  Address;
	private String  Country;
	private String  Email;
	private String  FaxNum;
	private String  CustomerId;
	private String  Name;
	private String  PhoneNum;
	private String  Surname;

	public Customer(){
		Name       = Client.askString("Enter Customers Name     > ");
		Surname    = Client.askString("Enter Customers Surname  > ");
		Country    = Client.askString("Enter Customers Country  > ");
		Address    = Client.askString("Enter Customers Address  > ");
		PhoneNum   = Client.askString("Enter Customers PhoneNum " +
			    " (Seperate multiple numbers with commas ','> ");
		FaxNum     = Client.askString("Enter Customers FaxNum   > ");
		Email      = Client.askString("Enter Customers Email    > ");
		CustomerId = Client.askString("Enter Customers id       > ");
	}	// End of constractor (remember.this)


	public String returnDbData(){
		return "INSERT INTO customer VALUES(' "+ this.Address +"', '"+
			this.Country +"', '"+ this.Email +"', '"+
			this.FaxNum +"', '"+ this.CustomerId +"', '"+
			this.Name +"', '"+ this.PhoneNum +"', '"+
			this.Surname +"')";
	}


	public static String selectFromCustomer(Statement stat){
		ArrayList<String> Customers = new ArrayList<>();
		try{
			ResultSet rs = stat.executeQuery("SELECT * FROM `customer`");
			while (rs.next())
				Customers.add(rs.getString("customerId"));
		}catch (Exception e){
			System.err.println("exception in selectFromCustomer\n" +
					e.getMessage() + "\n");
		}
		String input;
		do{
			System.out.println("\n" + Customers + "\n");
			input = Client.askString("Select customers id from above (Enter exit to quit)> ");
			if(input.equals("exit")){
				return null;
			}
		}while (!Customers.contains(input));
		return input;
	}


}	// End of class

```
\begin{center}\rule{4.8in}{0.4pt}\end{center}
\pagebreak


## Flight

Η Flight πέρα από το αντικείμενο και τον κατασκευαστή, έχει λειτουργίες
διατήρησης της πτήσης. Δηλαδή έχει ειδικούς ελέγχους για την εναλλαγή των
σημαιών και την διαχείριση των γεμάτων πτήσεων.

\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Java
class Flight{
	private Statement stat;
	private String BookingCity;
	private String Airline;
	private String ArrCity;
	private LocalDateTime ArrivalDateTime;
	private String Key;
	private int BussinessReserved;
	private int BussinessTotal;
	private int BussinessFlag;;
	private String DepartureCity;
	private LocalDateTime DepartureDateTime;
	private int EconomyReserved;
	private int EconomySeats;
	private int EconomyFlag;
	private int SmokingFlag;
	private double TicketPrice;

	public Flight(Statement stat){
		this.stat = stat;
		BookingCity       = selectFromBooking(stat);
		DepartureCity     = BookingCity;
		Airline           = fromCitytoAirline(stat, BookingCity);
		ArrCity           = Client.askString("Enter arrival city name > ");
		ArrivalDateTime   = Client.askDateTime("Enter arrival date and time");
		DepartureDateTime = Client.askDateTime("Enter departure date and time");
		Key               = Client.askString("Enter unique key identifier > ");
		// ask specs

		BussinessTotal    = Client.askInteger("Enter total bussines seats > ");
		BussinessReserved = 0;
		if ( BussinessReserved >= BussinessTotal ){
			BussinessFlag = 1;
		}else{
			BussinessFlag = 0;
		}

		EconomySeats      = Client.askInteger("Enter total economy seats > ");
		EconomyReserved   = 0;
		if ( EconomyReserved >= EconomySeats ){
			EconomyFlag = 1;
		}else{
			EconomyFlag = 0;
		}

		SmokingFlag       = Client.askInteger("Smoking? [1/0] > ");
		TicketPrice       = Client.askDouble("Enter Flight price > ");
	}	// End of constractor (remember.this)


	public String returnDbData(){
		return "INSERT INTO flight VALUES(" +
			"'" + this.Airline + "', " +
			"'" + this.ArrCity + "', " +
			"'" + this.ArrivalDateTime + "', " +
			"'" + this.BookingCity + "', " +
			" " + this.BussinessFlag + " , " +
			" " + this.BussinessReserved + " , " +
			" " + this.BussinessTotal + " , " +
			"'" + this.DepartureCity + "', " +
			"'" + this.DepartureDateTime + "', " +
			" " + this.EconomyFlag + " , " +
			" " + this.EconomyReserved + " , " +
			" " + this.EconomySeats + " , " +
			"'" + this.Key + "', " +
			" " + this.SmokingFlag + " , " +
			" " + this.TicketPrice + " )" ;

	}

	/*
	 * Functions
	 */


	public static String selectFromBooking(Statement stat){
		ArrayList<String> Citys = new ArrayList<>();
		try{
			ResultSet rs = stat.executeQuery("SELECT * FROM `booking`");
			while (rs.next())
				Citys.add(rs.getString("bookingCity"));
		}catch (Exception e){
			System.err.println("exception in selectFromBooking\n" +
					e.getMessage() + "\n");
		}
		String input;
		do{
			System.out.println("\n" + Citys + "\n");
			input = Client.askString("Enter office location from above  (Enter exit to quit)> ");
			if(input.equals("exit")){
				return null;
			}
		}while (!Citys.contains(input));
		return input;
	}


	public static String selectFromFlight(Statement stat){
		ArrayList<String> Flight = new ArrayList<>();
		try{
			ResultSet rs = stat.executeQuery("SELECT * FROM `flight`");
			while (rs.next())
				Flight.add(rs.getString("flightId"));
		}catch (Exception e){
			System.err.println("exception in selectFromFlight\n" +
					e.getMessage() + "\n");
		}
		String input;
		do{
			System.out.println("\n" + Flight + "\n");
			input = Client.askString("Enter flight from above (Enter exit to quit)> ");
			if(input.equals("exit")){
				return null;
			}
		}while (!Flight.contains(input));
		return input;
	}


	public static String selectFromAvBussinesFlights(Statement stat){
		ArrayList<String> Flight = new ArrayList<>();
		try{
			ResultSet rs = stat.executeQuery("SELECT * FROM `flight` WHERE `bussinessFlag` = 0");
			while (rs.next())
				Flight.add(rs.getString("flightId"));
		}catch (Exception e){
			System.err.println("exception in selectFromFlight\n" +
					e.getMessage() + "\n");
		}
		String input;
		do{
			System.out.println("\n" + Flight + "\n");
			input = Client.askString("Enter flight from above (Enter exit to quit)> ");
			if(input.equals("exit")){
				return null;
			}
		}while (!Flight.contains(input));
		return input;
	}


	public static String selectFromAvEconomyFlights(Statement stat){
		ArrayList<String> Flight = new ArrayList<>();
		try{
			ResultSet rs = stat.executeQuery("SELECT * FROM `flight` WHERE `economyFlag` = 0");
			while (rs.next())
				Flight.add(rs.getString("flightId"));
		}catch (Exception e){
			System.err.println("exception in selectFromFlight\n" +
					e.getMessage() + "\n");
		}
		String input;
		do{
			System.out.println("\n" + Flight + "\n");
			input = Client.askString("Enter flight from above (Enter exit to quit)> ");
			if(input.equals("exit")){
				return null;
			}
		}while (!Flight.contains(input));
		return input;
	}


	public static String fromCitytoAirline(Statement stat, String BookingCity){
		String Airline = "null";
		try{
			String Q ="SELECT * FROM `booking` WHERE `bookingCity` = '";
			ResultSet rs = stat.executeQuery(Q + BookingCity + "'");
			while (rs.next())
				Airline = rs.getString("Airline_airlineId");
		}catch (Exception asd){
			System.err.println("exception in fromCitytoAirline\n" +
					asd.getMessage() + "\n");
		}
		return Airline;
	}


	public static String fromAirlinetoCountry(Statement stat, String airline){
		String country = "null";
		try{
			String Q ="SELECT * FROM `airline` WHERE `airlineId` = '";
			ResultSet rs = stat.executeQuery(Q + airline + "'");
			while (rs.next())
				country = rs.getString("Country_countryName");
		}catch (Exception asd){
			System.err.println("exception in fromAirlinetoCountry\n" +
					asd.getMessage() + "\n");
		}
		return country;
	}


	public static void checkFullFlights(Statement stat){
		try{
			ResultSet rs = stat.executeQuery("SELECT `flightId` FROM `flight` WHERE `economyFlag`= 1");
			while(rs.next())
				System.out.println("No available economy Seats in : " + rs.getString("flightId") + "\n");
			rs = stat.executeQuery("SELECT `flightId` FROM `flight` WHERE `bussinessFlag`= 1");
			while(rs.next())
				System.out.println("No available bussiness Seats in : " + rs.getString("flightId") + "\n");
		}catch(Exception e){
			System.err.println("exception in checkFullFlights\n" +
					e.getMessage() + "\n");
		}
	}


	public static void checkFlights(Statement stat){
		try{
			ResultSet rs = stat.executeQuery("SELECT `flightId` FROM `flight` WHERE 1");
			while(rs.next())
				System.out.println("Flight : " + rs.getString("flightId") + "\n");
		}catch(Exception e){
			System.out.println("Exception in checkFlights");
		}
	}


}	// End of class

```
\begin{center}\rule{4.8in}{0.4pt}\end{center}
\pagebreak


## Ticket

Η Ticket αντίστοιχα αρχικοποιεί ένα εισιτήριο άλλα πέρα από της μεθόδους για
την διεπαφή με τον χρήστη έχει διάφορες μεθόδους για την διαχείριση των ξένων
νομισμάτων. Επίσης έχει την δουλεία να μειώνει και να ελέγχει της θέσεις
στις πτήσης καθώς δημιουργούνται εισιτήρια.

\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Java
class Ticket{
	private Statement stat;
	private String ArrivalDateTime;
	private String CityName;
	private String DepartureDateTime;
	private double FlightPrice;
	private double FlightPriceLocal;
	private double PriceDeposited;
	private double PriceRemaining;
	private double TicketBussinessFlag;
	private LocalDateTime TicketDate;
	private String TicketId;
	private String TicketStatus;
	private double TotalPrice;
	private double TotalPriceLocal;
	private String Flight_flightId;
	private String Customer_customerId;
	private double Tax;
	private double Exrate;


	public Ticket(Statement stat){
		TicketBussinessFlag = Client.askInteger("Is the seat in bussines class? [1/0] > ");
		if( TicketBussinessFlag == 1){
			Flight_flightId = Flight.selectFromAvBussinesFlights(stat);
			decreaseFlightBussinessSeat(stat, Flight_flightId);
		}else{
			Flight_flightId = Flight.selectFromAvEconomyFlights(stat);
			decreaseFlightEconomySeat(stat, Flight_flightId);
		}
		Customer_customerId = Customer.selectFromCustomer(stat);
		CityName = Flight.selectFromBooking(stat);
		TicketId = Client.askString("Enter ticket id > ");
		TicketStatus = "c";
		// Time of ticket creation
		TicketDate = LocalDateTime.now();
		PriceDeposited = 0;
		try {
			ResultSet rs = stat.executeQuery("SELECT * FROM `flight` WHERE flightId = '" + Flight_flightId + "'");
			while(rs.next()){
				// Ticket price
				FlightPrice = rs.getDouble("ticketPrice");
				// Time and date
				DepartureDateTime = rs.getString("departureDateTime");
				ArrivalDateTime =  rs.getString("arrivalDateTime");
			}
		}catch(Exception e){
			System.out.println("Error in ticket constractor flight selection" + e.getMessage());
		}
		//PROBLEM: Coustomer can be from anywere, cannot find his currency
		//COPE: I will use currency of the country were the ticket was initialised
		String airline = Flight.fromCitytoAirline(stat, CityName);
		String country = Flight.fromAirlinetoCountry(stat, airline);
		Tax = fromCountrytoTax(stat, country);
		Exrate = fromCountrytoExchangeRate(stat, country);
		TotalPrice = FlightPrice + Tax;
		TotalPriceLocal = TotalPrice * Exrate;
		FlightPriceLocal = FlightPrice * Exrate;
		PriceRemaining = TotalPrice - PriceDeposited;
	}


	public String returnDbData(){
		return "INSERT INTO ticket VALUES( " +
			"'" + this.ArrivalDateTime + "', " +
			"'" + this.CityName + "', " +
			"'" + this.DepartureDateTime + "', " +
			" " + this.FlightPrice + " , " +
			" " + this.FlightPriceLocal + " , " +
			" " + this.PriceDeposited + " , " +
			" " + this.PriceRemaining + " , " +
			" " + this.TicketBussinessFlag + " , " +
			"'" + this.TicketDate + "', " +
			"'" + this.TicketId + "', " +
			"'" + this.TicketStatus + "', " +
			" " + this.TotalPrice + " , " +
			" " + this.TotalPriceLocal + " , " +
			"'" + this.Flight_flightId + "', " +
			"'" + this.Customer_customerId + "')";
	}


	public Double fromCountrytoExchangeRate(Statement stat, String country){
		double Exrate = 0.0;
		try{
			String Q = "SELECT `exchangeRate` FROM `country` WHERE `countryName` = '";
			ResultSet rs = stat.executeQuery(Q + country + "'");
			while (rs.next())
				Exrate = rs.getDouble("exchangeRate");
		}catch (Exception aa){
			System.err.println("exception in fromCountrytoExchangeRate \n" +
					aa.getMessage() + "\n");
		}
		return Exrate;
	}


	public Double fromCountrytoTax(Statement stat, String country){
		double Tax = 0.0;
		try{
			String Q = "SELECT `tax` FROM `country` WHERE `countryName` = '";
			ResultSet rs = stat.executeQuery(Q + country + "'");
			while (rs.next())
				Tax = rs.getDouble("tax");
		}catch (Exception aa){
			System.err.println("exception in fromCountrytoExchangeRate \n" +
					aa.getMessage() + "\n");
		}
		return Tax;
	}


	public void decreaseFlightEconomySeat(Statement stat, String flight){
		try{
			stat.executeUpdate("UPDATE flight SET economyReserved = economyReserved -1 WHERE flightId = '" + flight + "'");
			ResultSet rs = stat.executeQuery("SELECT * FROM `flight` WHERE `flightId` = '" + flight +"'");
			int res = 0;
			int total = 0;
			while(rs.next()){
				res = rs.getInt("economyReserved");
				total = rs.getInt("economySeats");
			}
			if (res >= total){
				stat.executeUpdate("UPDATE `flight` SET `economyFlag` = 1 WHERE `flightId` = '"+ flight +"'");
			}
		}catch(Exception e){
			System.out.println("Exception in decreaseFlightEconomySeat");
		}
	}


	public void decreaseFlightBussinessSeat(Statement stat, String flight){
		try{
			stat.executeUpdate("UPDATE flight SET bussinessReserved = bussinessReserved -1 WHERE flightId = '" + flight + "'");
			ResultSet rs = stat.executeQuery("SELECT * FROM `flight` WHERE `flightId` = '" + flight +"'");
			int res = 0;
			int total = 0;
			while(rs.next()){
				res = rs.getInt("bussinessReserved");
				total = rs.getInt("bussinessTotal");
			}
			if (res >= total){
				stat.executeUpdate("UPDATE `flight` SET `bussinessFlag` = 1 WHERE `flightId` = '"+ flight +"'");
			}
		}catch(Exception e){
			System.out.println("Exception in decreaseFlightbussinessSeat");
		}
	}


}	// End of class
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}
\pagebreak

## PaymentPortal

Και τέλος η PaymentPortal, που μπορεί να τρέξει ανεξάρτητα από το μενού του
Client, δηλαδή (αν είχε στοιχειώδεις μηχανισμούς αυθεντικόποίησης) θα μπορούσε
να τρέξει αυτόνομα από τον χρήστη και να έχει πρόσβαση στα εισιτήρια και στον
τρόπο να τα πληρώσει - ακυρώσει.

\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Java
class PaymentPortal{
	public static void main(){

		/*
		 * Sql Connection
		 */

		Connection conn = null;
		String url = "jdbc:mysql://192.168.2.113:3306/";
		String dbName = "airport_base";
		String driver = "com.mysql.jdbc.Driver";
		String userName = "herk";
		String password = "2616";

		/*
		 * Sql Driver
		 */

		try{
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url+dbName,userName,password);
			System.out.println("Connected to database");
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("Connection failed");
		}
		try{
			Statement stat = conn.createStatement();
			stat.execute("SET FOREIGN_KEY_CHECKS = 0 ");
			int inp         = 0;
			int exitTrigger = 1;
			while ( exitTrigger == 1 ) {
				System.out.println(
" _ __                                  _ __                 _   \n" +
"' )  )                           _/_  ' )  )      _/_      //   \n" +
" /--'__.  __  , ______  _  ____  /     /--'__ __  /  __.  //    \n" +
"/   (_/|_/ (_/_/ / / <_</_/ / <_<__   /   (_)/ (_<__(_/|_</_    \n" +
"            /                                                   \n" +
"           '                                                    \n" +
"                                                                \n" +
"     1. To check your ticket - tikets                           \n" +
"     2. To pay for a ticket                                     \n" +
"     3. To cancel a ticket                                      \n" +
"     4. Delete canceled tickets                                 \n" +
"                                                                \n" +
"     0. Exit                                                    \n" +
"                                                                \n");
				Scanner scan = new Scanner(System.in);
				inp = Client.isMenuOption();

				/*
				 * Selection
				 */

				switch (inp){
					case 0: // Opt 0 Exit
						Client.clearScreen();
						exitTrigger = 0;
						break;
					case 1: // Opt1 ViewTIcket
						viewTicket(stat);
						break;
					case 2: // Opt2 payForTicket
						payForTicket(stat);
						Client.clearScreen();
						break;
					case 3: // Opt3 cancelATicket
						cancelATicket(stat);
						Client.clearScreen();
						break;
					case 4: // Opt4 deleteCanceledTickets
						deleteCanceledTickets(stat);
						break;
					default :
						Client.clearScreen();
						break;
				} 	// End of switch
			} 	// End of while
			}catch(Exception e){
				System.out.println("Connection Failed" +
						e.getMessage() );
			}
	}	// End of main


	public static String selectFromCustomer(Statement stat){
		ArrayList<String> Customers = new ArrayList<>();
		try{
			ResultSet rs = stat.executeQuery("SELECT * FROM `customer`");
			while (rs.next())
				Customers.add(rs.getString("customerId"));
		}catch (Exception e){
			System.err.println("exception in selectFromCustomer\n" +
					e.getMessage() + "\n");
		}
		String input;
		do{
			System.out.println("\n" + Customers + "\n");
			input = Client.askString("Select customer id from above  (Enter exit to quit)> ");
			if(input.equals("exit")){
				return null;
			}
		}while (!Customers.contains(input));
		return input;
	}


	public static String selectFromTicket(Statement stat, String customer){
		ArrayList<String> Tickets = new ArrayList<>();
		try{
			ResultSet rs = stat.executeQuery("SELECT * FROM `ticket` WHERE `Customer_customerId` = '" +
					customer + "' ");
			while (rs.next())
				Tickets.add(rs.getString("ticketId"));
		}catch (Exception e){
			System.err.println("exception in selectFromCustomer\n" +
					e.getMessage() + "\n");
		}
		String input;
		do{
			System.out.println("\n" + Tickets + "\n");
			input = Client.askString("Select ticket from above  (Enter exit to quit)> ");
			if(input.equals("exit")){
				return null;
			}
		}while (!Tickets.contains(input));
		return input;
	}


	public static void viewTicket(Statement stat){
		String customer = selectFromCustomer(stat);
		String ticket = selectFromTicket(stat, customer);
		try{
			ResultSet rs = stat.executeQuery("SELECT * FROM " +
					"`ticket` WHERE `ticketId` = '" + ticket + "'");
			ResultSetMetaData rsmd = rs.getMetaData();
			rs.next();
			for(int i = 1; i <= 15; i++){
				System.out.println(rsmd.getColumnName(i) + "   > " + rs.getString(i) + "  ");
			}
			Client.pressEnter();
		}catch(Exception e){
			System.out.println("Exception in viewTicket" +
					e.getMessage() + "\n");
		}
	}


	public static void payForTicket(Statement stat){
		String customer = selectFromCustomer(stat);
		String ticket = selectFromTicket(stat, customer);
		try{
			double priceRemaining = 0;
			ResultSet tk = stat.executeQuery("SELECT * FROM `ticket` WHERE `ticketId` = '" + ticket + "'");
			while(tk.next())
				priceRemaining = tk.getDouble("priceRemaining");
			double depositPrice = 0;
			do{
				depositPrice = Client.askDouble("Enter money to deposit. (remaining : " + priceRemaining + " ) ");
				if (depositPrice > priceRemaining){
					System.out.println("ERROR: Cannot deposit more than the ticket price \n");
				}
			}while(depositPrice > priceRemaining);
			stat.executeUpdate("UPDATE `ticket` SET `priceDeposited` = `priceDeposited` + " +
					depositPrice + " , `priceRemaining` = `totalPrice` - `priceDeposited` WHERE `ticketId` = '" +
					ticket +"'");
			if(priceRemaining - depositPrice == 0){
				// Paid
				stat.executeUpdate("UPDATE `ticket` SET `ticketStatus` = 'a' WHERE `ticketId` = '" +
					ticket +"'");
			}else{
				// Partially paid
				stat.executeUpdate("UPDATE `ticket` SET `ticketStatus` = 'c' WHERE `ticketId` = '" +
					ticket +"'");
			}
		}catch(Exception e){
			System.err.println("exception in payForTicket\n" +
					e.getMessage() + "\n");
		}
	}


	public static double cancelATicket(Statement stat){
		String customer = selectFromCustomer(stat);
		String ticket = selectFromTicket(stat, customer);
		double priceDeposited = 0;
		try{
			stat.executeUpdate("UPDATE `ticket` SET `ticketStatus` = 'b' WHERE `ticketId` = '" +
					ticket +"'");
			ResultSet tk = stat.executeQuery("SELECT * FROM `ticket` WHERE `ticketId` = '" + ticket + "'");
			while(tk.next())
				priceDeposited = tk.getDouble("priceDeposited");
			System.out.println("Refunding "+ priceDeposited  +" to {"+ customer +"}");
		}catch(Exception e){
			System.err.println("exception in cancelATicket\n" +
					e.getMessage() + "\n");
		}
		return priceDeposited;
	}


	public static void deleteCanceledTickets(Statement stat){
		try{
			ArrayList<String> ticketToDel = new ArrayList<>();
			ResultSet rn = stat.executeQuery("SELECT * FROM `ticket` WHERE `ticketStatus` = 'b'");
			while(rn.next())
				ticketToDel.add(rn.getString("ticketId"));
			for(int i=0; i < ticketToDel.size(); i++){
				String ttd = ticketToDel.get(i);
				System.out.println("Deleting -> -" + ttd + "- ");
				stat.executeUpdate("DELETE FROM `ticket` WHERE `ticketId` = '" + ttd + "'");
			}
		}catch(Exception e){
			System.err.println("exception in deleteCanceledTickets\n" +
					e.getMessage() + "\n");
		}
	}


}	// End of class
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}
\pagebreak
