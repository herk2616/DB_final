import java.sql.*;
import java.util.Scanner;
import java.util.ArrayList;
import java.time.LocalDateTime;

class PaymentPortal{
	public static void main(){

		/*
		 * Sql Connection
		 */

		Connection conn = null;
		String url = "jdbc:mysql://192.168.2.113:3306/";
		String dbName = "airport_base";
		String driver = "com.mysql.jdbc.Driver";
		String userName = "herk";
		String password = "2616";

		/*
		 * Sql Driver
		 */

		try{
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url+dbName,userName,password);
			System.out.println("Connected to database");
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("Connection failed");
		}
		try{
			Statement stat = conn.createStatement();
			stat.execute("SET FOREIGN_KEY_CHECKS = 0 ");
			int inp         = 0;
			int exitTrigger = 1;
			while ( exitTrigger == 1 ) {
				System.out.println(
" _ __                                  _ __                 _   \n" +
"' )  )                           _/_  ' )  )      _/_      //   \n" +
" /--'__.  __  , ______  _  ____  /     /--'__ __  /  __.  //    \n" +
"/   (_/|_/ (_/_/ / / <_</_/ / <_<__   /   (_)/ (_<__(_/|_</_    \n" +
"            /                                                   \n" +
"           '                                                    \n" +
"                                                                \n" +
"     1. To check your ticket - tikets                           \n" +
"     2. To pay for a ticket                                     \n" +
"     3. To cancel a ticket                                      \n" +
"     4. Delete canceled tickets                                 \n" +
"                                                                \n" +
"     0. Exit                                                    \n" +
"                                                                \n");
				Scanner scan = new Scanner(System.in);
				inp = Client.isMenuOption();

				/*
				 * Selection
				 */

				switch (inp){
					case 0: // Opt 0 Exit
						Client.clearScreen();
						exitTrigger = 0;
						break;
					case 1: // Opt1 ViewTIcket
						viewTicket(stat);
						break;
					case 2: // Opt2 payForTicket
						payForTicket(stat);
						Client.clearScreen();
						break;
					case 3: // Opt3 cancelATicket
						cancelATicket(stat);
						Client.clearScreen();
						break;
					case 4: // Opt4 deleteCanceledTickets
						deleteCanceledTickets(stat);
						break;
					default :
						Client.clearScreen();
						break;
				} 	// End of switch
			} 	// End of while
			}catch(Exception e){
				System.out.println("Connection Failed" +
						e.getMessage() );
			}
	}	// End of main


	public static String selectFromCustomer(Statement stat){
		ArrayList<String> Customers = new ArrayList<>();
		try{
			ResultSet rs = stat.executeQuery("SELECT * FROM `customer`");
			while (rs.next())
				Customers.add(rs.getString("customerId"));
		}catch (Exception e){
			System.err.println("exception in selectFromCustomer\n" +
					e.getMessage() + "\n");
		}
		String input;
		do{
			System.out.println("\n" + Customers + "\n");
			input = Client.askString("Select customer id from above  (Enter exit to quit)> ");
			if(input.equals("exit")){
				return null;
			}
		}while (!Customers.contains(input));
		return input;
	}


	public static String selectFromTicket(Statement stat, String customer){
		ArrayList<String> Tickets = new ArrayList<>();
		try{
			ResultSet rs = stat.executeQuery("SELECT * FROM `ticket` WHERE `Customer_customerId` = '" +
					customer + "' ");
			while (rs.next())
				Tickets.add(rs.getString("ticketId"));
		}catch (Exception e){
			System.err.println("exception in selectFromCustomer\n" +
					e.getMessage() + "\n");
		}
		String input;
		do{
			System.out.println("\n" + Tickets + "\n");
			input = Client.askString("Select ticket from above  (Enter exit to quit)> ");
			if(input.equals("exit")){
				return null;
			}
		}while (!Tickets.contains(input));
		return input;
	}


	public static void viewTicket(Statement stat){
		String customer = selectFromCustomer(stat);
		String ticket = selectFromTicket(stat, customer);
		try{
			ResultSet rs = stat.executeQuery("SELECT * FROM " +
					"`ticket` WHERE `ticketId` = '" + ticket + "'");
			ResultSetMetaData rsmd = rs.getMetaData();
			rs.next();
			for(int i = 1; i <= 15; i++){
				System.out.println(rsmd.getColumnName(i) + "   > " + rs.getString(i) + "  ");
			}
			Client.pressEnter();
		}catch(Exception e){
			System.out.println("Exception in viewTicket" +
					e.getMessage() + "\n");
		}
	}


	public static void payForTicket(Statement stat){
		String customer = selectFromCustomer(stat);
		String ticket = selectFromTicket(stat, customer);
		try{
			double priceRemaining = 0;
			ResultSet tk = stat.executeQuery("SELECT * FROM `ticket` WHERE `ticketId` = '" + ticket + "'");
			while(tk.next())
				priceRemaining = tk.getDouble("priceRemaining");
			double depositPrice = 0;
			do{
				depositPrice = Client.askDouble("Enter money to deposit. (remaining : " + priceRemaining + " ) ");
				if (depositPrice > priceRemaining){
					System.out.println("ERROR: Cannot deposit more than the ticket price \n");
				}
			}while(depositPrice > priceRemaining);
			stat.executeUpdate("UPDATE `ticket` SET `priceDeposited` = `priceDeposited` + " +
					depositPrice + " , `priceRemaining` = `totalPrice` - `priceDeposited` WHERE `ticketId` = '" +
					ticket +"'");
			if(priceRemaining - depositPrice == 0){
				// Paid
				stat.executeUpdate("UPDATE `ticket` SET `ticketStatus` = 'a' WHERE `ticketId` = '" +
					ticket +"'");
			}else{
				// Partially paid
				stat.executeUpdate("UPDATE `ticket` SET `ticketStatus` = 'c' WHERE `ticketId` = '" +
					ticket +"'");
			}
		}catch(Exception e){
			System.err.println("exception in payForTicket\n" +
					e.getMessage() + "\n");
		}
	}


	public static double cancelATicket(Statement stat){
		String customer = selectFromCustomer(stat);
		String ticket = selectFromTicket(stat, customer);
		double priceDeposited = 0;
		try{
			stat.executeUpdate("UPDATE `ticket` SET `ticketStatus` = 'b' WHERE `ticketId` = '" +
					ticket +"'");
			ResultSet tk = stat.executeQuery("SELECT * FROM `ticket` WHERE `ticketId` = '" + ticket + "'");
			while(tk.next())
				priceDeposited = tk.getDouble("priceDeposited");
			System.out.println("Refunding "+ priceDeposited  +" to {"+ customer +"}");
		}catch(Exception e){
			System.err.println("exception in cancelATicket\n" +
					e.getMessage() + "\n");
		}
		return priceDeposited;
	}


	public static void deleteCanceledTickets(Statement stat){
		try{
			ArrayList<String> ticketToDel = new ArrayList<>();
			ResultSet rn = stat.executeQuery("SELECT * FROM `ticket` WHERE `ticketStatus` = 'b'");
			while(rn.next())
				ticketToDel.add(rn.getString("ticketId"));
			for(int i=0; i < ticketToDel.size(); i++){
				String ttd = ticketToDel.get(i);
				System.out.println("Deleting -> -" + ttd + "- ");
				stat.executeUpdate("DELETE FROM `ticket` WHERE `ticketId` = '" + ttd + "'");
			}
		}catch(Exception e){
			System.err.println("exception in deleteCanceledTickets\n" +
					e.getMessage() + "\n");
		}
	}


}	// End of class
