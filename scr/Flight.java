import java.sql.*;
import java.util.Scanner;
import java.util.ArrayList;
import java.time.LocalDateTime;

class Flight{
	private Statement stat;
	private String BookingCity;
	private String Airline;
	private String ArrCity;
	private LocalDateTime ArrivalDateTime;
	private String Key;
	private int BussinessReserved;
	private int BussinessTotal;
	private int BussinessFlag;;
	private String DepartureCity;
	private LocalDateTime DepartureDateTime;
	private int EconomyReserved;
	private int EconomySeats;
	private int EconomyFlag;
	private int SmokingFlag;
	private double TicketPrice;

	public Flight(Statement stat){
		this.stat = stat;
		BookingCity       = selectFromBooking(stat);
		DepartureCity     = BookingCity;
		Airline           = fromCitytoAirline(stat, BookingCity);
		ArrCity           = Client.askString("Enter arrival city name > ");
		ArrivalDateTime   = Client.askDateTime("Enter arrival date and time");
		DepartureDateTime = Client.askDateTime("Enter departure date and time");
		Key               = Client.askString("Enter unique key identifier > ");
		// ask specs

		BussinessTotal    = Client.askInteger("Enter total bussines seats > ");
		BussinessReserved = 0;
		if ( BussinessReserved >= BussinessTotal ){
			BussinessFlag = 1;
		}else{
			BussinessFlag = 0;
		}

		EconomySeats      = Client.askInteger("Enter total economy seats > ");
		EconomyReserved   = 0;
		if ( EconomyReserved >= EconomySeats ){
			EconomyFlag = 1;
		}else{
			EconomyFlag = 0;
		}

		SmokingFlag       = Client.askInteger("Smoking? [1/0] > ");
		TicketPrice       = Client.askDouble("Enter Flight price > ");
	}	// End of constractor (remember.this)


	public String returnDbData(){
		return "INSERT INTO flight VALUES(" +
			"'" + this.Airline + "', " +
			"'" + this.ArrCity + "', " +
			"'" + this.ArrivalDateTime + "', " +
			"'" + this.BookingCity + "', " +
			" " + this.BussinessFlag + " , " +
			" " + this.BussinessReserved + " , " +
			" " + this.BussinessTotal + " , " +
			"'" + this.DepartureCity + "', " +
			"'" + this.DepartureDateTime + "', " +
			" " + this.EconomyFlag + " , " +
			" " + this.EconomyReserved + " , " +
			" " + this.EconomySeats + " , " +
			"'" + this.Key + "', " +
			" " + this.SmokingFlag + " , " +
			" " + this.TicketPrice + " )" ;

	}

	/*
	 * Functions
	 */


	public static String selectFromBooking(Statement stat){
		ArrayList<String> Citys = new ArrayList<>();
		try{
			ResultSet rs = stat.executeQuery("SELECT * FROM `booking`");
			while (rs.next())
				Citys.add(rs.getString("bookingCity"));
		}catch (Exception e){
			System.err.println("exception in selectFromBooking\n" +
					e.getMessage() + "\n");
		}
		String input;
		do{
			System.out.println("\n" + Citys + "\n");
			input = Client.askString("Enter office location from above  (Enter exit to quit)> ");
			if(input.equals("exit")){
				return null;
			}
		}while (!Citys.contains(input));
		return input;
	}


	public static String selectFromFlight(Statement stat){
		ArrayList<String> Flight = new ArrayList<>();
		try{
			ResultSet rs = stat.executeQuery("SELECT * FROM `flight`");
			while (rs.next())
				Flight.add(rs.getString("flightId"));
		}catch (Exception e){
			System.err.println("exception in selectFromFlight\n" +
					e.getMessage() + "\n");
		}
		String input;
		do{
			System.out.println("\n" + Flight + "\n");
			input = Client.askString("Enter flight from above (Enter exit to quit)> ");
			if(input.equals("exit")){
				return null;
			}
		}while (!Flight.contains(input));
		return input;
	}


	public static String selectFromAvBussinesFlights(Statement stat){
		ArrayList<String> Flight = new ArrayList<>();
		try{
			ResultSet rs = stat.executeQuery("SELECT * FROM `flight` WHERE `bussinessFlag` = 0");
			while (rs.next())
				Flight.add(rs.getString("flightId"));
		}catch (Exception e){
			System.err.println("exception in selectFromFlight\n" +
					e.getMessage() + "\n");
		}
		String input;
		do{
			System.out.println("\n" + Flight + "\n");
			input = Client.askString("Enter flight from above (Enter exit to quit)> ");
			if(input.equals("exit")){
				return null;
			}
		}while (!Flight.contains(input));
		return input;
	}


	public static String selectFromAvEconomyFlights(Statement stat){
		ArrayList<String> Flight = new ArrayList<>();
		try{
			ResultSet rs = stat.executeQuery("SELECT * FROM `flight` WHERE `economyFlag` = 0");
			while (rs.next())
				Flight.add(rs.getString("flightId"));
		}catch (Exception e){
			System.err.println("exception in selectFromFlight\n" +
					e.getMessage() + "\n");
		}
		String input;
		do{
			System.out.println("\n" + Flight + "\n");
			input = Client.askString("Enter flight from above (Enter exit to quit)> ");
			if(input.equals("exit")){
				return null;
			}
		}while (!Flight.contains(input));
		return input;
	}


	public static String fromCitytoAirline(Statement stat, String BookingCity){
		String Airline = "null";
		try{
			String Q ="SELECT * FROM `booking` WHERE `bookingCity` = '";
			ResultSet rs = stat.executeQuery(Q + BookingCity + "'");
			while (rs.next())
				Airline = rs.getString("Airline_airlineId");
		}catch (Exception asd){
			System.err.println("exception in fromCitytoAirline\n" +
					asd.getMessage() + "\n");
		}
		return Airline;
	}


	public static String fromAirlinetoCountry(Statement stat, String airline){
		String country = "null";
		try{
			String Q ="SELECT * FROM `airline` WHERE `airlineId` = '";
			ResultSet rs = stat.executeQuery(Q + airline + "'");
			while (rs.next())
				country = rs.getString("Country_countryName");
		}catch (Exception asd){
			System.err.println("exception in fromAirlinetoCountry\n" +
					asd.getMessage() + "\n");
		}
		return country;
	}


	public static void checkFullFlights(Statement stat){
		try{
			ResultSet rs = stat.executeQuery("SELECT `flightId` FROM `flight` WHERE `economyFlag`= 1");
			while(rs.next())
				System.out.println("No available economy Seats in : " + rs.getString("flightId") + "\n");
			rs = stat.executeQuery("SELECT `flightId` FROM `flight` WHERE `bussinessFlag`= 1");
			while(rs.next())
				System.out.println("No available bussiness Seats in : " + rs.getString("flightId") + "\n");
		}catch(Exception e){
			System.err.println("exception in checkFullFlights\n" +
					e.getMessage() + "\n");
		}
	}


	public static void checkFlights(Statement stat){
		try{
			ResultSet rs = stat.executeQuery("SELECT `flightId` FROM `flight` WHERE 1");
			while(rs.next())
				System.out.println("Flight : " + rs.getString("flightId") + "\n");
		}catch(Exception e){
			System.out.println("Exception in checkFlights");
		}
	}


}	// End of class
