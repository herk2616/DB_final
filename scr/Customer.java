import java.sql.*;
import java.util.Scanner;
import java.util.ArrayList;
import java.time.LocalDateTime;

class Customer{
	private String  Address;
	private String  Country;
	private String  Email;
	private String  FaxNum;
	private String  CustomerId;
	private String  Name;
	private String  PhoneNum;
	private String  Surname;

	public Customer(){
		Name       = Client.askString("Enter Customers Name     > ");
		Surname    = Client.askString("Enter Customers Surname  > ");
		Country    = Client.askString("Enter Customers Country  > ");
		Address    = Client.askString("Enter Customers Address  > ");
		PhoneNum   = Client.askString("Enter Customers PhoneNum " +
			    " (Seperate multiple numbers with commas ','> ");
		FaxNum     = Client.askString("Enter Customers FaxNum   > ");
		Email      = Client.askString("Enter Customers Email    > ");
		CustomerId = Client.askString("Enter Customers id       > ");
	}	// End of constractor (remember.this)


	public String returnDbData(){
		return "INSERT INTO customer VALUES(' "+ this.Address +"', '"+
			this.Country +"', '"+ this.Email +"', '"+
			this.FaxNum +"', '"+ this.CustomerId +"', '"+
			this.Name +"', '"+ this.PhoneNum +"', '"+
			this.Surname +"')";
	}


	public static String selectFromCustomer(Statement stat){
		ArrayList<String> Customers = new ArrayList<>();
		try{
			ResultSet rs = stat.executeQuery("SELECT * FROM `customer`");
			while (rs.next())
				Customers.add(rs.getString("customerId"));
		}catch (Exception e){
			System.err.println("exception in selectFromCustomer\n" +
					e.getMessage() + "\n");
		}
		String input;
		do{
			System.out.println("\n" + Customers + "\n");
			input = Client.askString("Select customers id from above (Enter exit to quit)> ");
			if(input.equals("exit")){
				return null;
			}
		}while (!Customers.contains(input));
		return input;
	}


}	// End of class
