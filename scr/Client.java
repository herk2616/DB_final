// Imports zR-openall zM-closeall
import java.sql.*;
import java.util.Scanner;
import java.util.ArrayList;
import java.time.LocalDateTime;

class Client{
	public static void main(String[] args){

		/*
		 * Sql Connection
		 */

		Connection conn = null;
		String url = "jdbc:mysql://192.168.2.113:3306/";
		String dbName = "airport_base";
		String driver = "com.mysql.jdbc.Driver";
		String userName = "herk";
		String password = "2616";

		/*
		 * Sql Driver
		 */

		try{
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url+dbName,userName,password);
			System.out.println("Connected to database");
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("Connection failed");
		}

		try{
			Statement stat = conn.createStatement();
			stat.execute("SET FOREIGN_KEY_CHECKS = 0 ");

			// Drop all tables
			stat.executeUpdate("DELETE FROM country WHERE 1");
			stat.executeUpdate("DELETE FROM airline WHERE 1");
			stat.executeUpdate("DELETE FROM booking WHERE 1");
			stat.executeUpdate("DELETE FROM flight WHERE 1");
			stat.executeUpdate("DELETE FROM ticket WHERE 1");
			stat.executeUpdate("DELETE FROM customer WHERE 1");

			// Countys
			stat.executeUpdate("INSERT INTO country VALUES('Canada', 'CAD', 1.53, 1)");
			stat.executeUpdate("INSERT INTO country VALUES('Usa', 'USD', 1.11, 1)");
			stat.executeUpdate("INSERT INTO country VALUES('Italy', 'ITL', 2000, 1500)");
			stat.executeUpdate("INSERT INTO country VALUES('Britain', 'GBP', 0.90, 1)");
			stat.executeUpdate("INSERT INTO country VALUES('France', 'FRF', 5.89, 5)");
			stat.executeUpdate("INSERT INTO country VALUES('Germany', 'DEM', 1.76, 2)");

			// Airline
			stat.executeUpdate("INSERT INTO airline VALUES('AirCan', 'Canada')");
			stat.executeUpdate("INSERT INTO airline VALUES('USAir', 'Usa')");
			stat.executeUpdate("INSERT INTO airline VALUES('ItalAir', 'Italy')");
			stat.executeUpdate("INSERT INTO airline VALUES('BritAir', 'Britain')");
			stat.executeUpdate("INSERT INTO airline VALUES('AirFrance', 'France')");
			stat.executeUpdate("INSERT INTO airline VALUES('LuftAir', 'Germany')");

			// Booking agencies
			stat.executeUpdate("INSERT INTO booking VALUES('Toronto', 'AirCan')");
			stat.executeUpdate("INSERT INTO booking VALUES('Montreal', 'AirCan')");
			stat.executeUpdate("INSERT INTO booking VALUES('New york', 'USAir')");
			stat.executeUpdate("INSERT INTO booking VALUES('Sicago', 'USAir')");
			stat.executeUpdate("INSERT INTO booking VALUES('Rome', 'ItalAir')");
			stat.executeUpdate("INSERT INTO booking VALUES('Napole', 'ItalAir')");
			stat.executeUpdate("INSERT INTO booking VALUES('London', 'BritAir')");
			stat.executeUpdate("INSERT INTO booking VALUES('Emdiburg', 'BritAir')");
			stat.executeUpdate("INSERT INTO booking VALUES('Paris', 'AirFrance')");
			stat.executeUpdate("INSERT INTO booking VALUES('Nikaia', 'AirFrance')");
			stat.executeUpdate("INSERT INTO booking VALUES('Bonne', 'LuftAir')");
			stat.executeUpdate("INSERT INTO booking VALUES('Berlin', 'LuftAir')");

			//Flight
			stat.executeUpdate("INSERT INTO flight VALUES('AirCan', 'New york', '2020-09-21 21:25:00', 'Toronto', 0, 5, 30, 'Toronto', '2020-09-21 21:23:00', 0, 30, 150, 'fl1', 0, 200 )");
			stat.executeUpdate("INSERT INTO flight VALUES('AirCan', 'New york', '2020-09-21 21:25:00', 'Toronto', 0, 5, 30, 'Toronto', '2020-09-21 21:23:00', 0, 30, 150, 'fl2', 0, 200 )");
			stat.executeUpdate("INSERT INTO flight VALUES('AirCan', 'New york', '2020-09-21 21:25:00', 'Toronto', 0, 5, 30, 'Toronto', '2020-09-21 21:23:00', 0, 30, 150, 'fl3', 0, 200 )");

			// Customer
			stat.executeUpdate("INSERT INTO customer VALUES('address', 'country', 'email', 'fax', 'hm1', 'name', 'phone1, phone2', 'surname')");
			stat.executeUpdate("INSERT INTO customer VALUES('address', 'country', 'email', 'fax', 'hm2', 'name', 'phone1, phone2', 'surname')");
			stat.executeUpdate("INSERT INTO customer VALUES('address', 'country', 'email', 'fax', 'hm3', 'name', 'phone1, phone2', 'surname')");

			//Ticket
			stat.executeUpdate("INSERT INTO ticket VALUES( '2020-02-02T02:03:02', 'Toronto', '2020-02-02T02:02:02',  200.0 ,  306.0 ,  0.0 ,  201.0 ,  0.0 , '2020-06-07T02:44:05.392777', 'tk1', 'c',  201.0 ,  307.53000000000003 , 'fl1', 'hm1')");
			stat.executeUpdate("INSERT INTO ticket VALUES( '2020-02-02T02:03:02', 'Toronto', '2020-02-02T02:02:02',  200.0 ,  306.0 ,  0.0 ,  201.0 ,  0.0 , '2020-06-07T02:44:05.392777', 'tk2', 'c',  201.0 ,  307.53000000000003 , 'fl1', 'hm1')");
			stat.executeUpdate("INSERT INTO ticket VALUES( '2020-02-02T02:03:02', 'Toronto', '2020-02-02T02:02:02',  200.0 ,  306.0 ,  0.0 ,  201.0 ,  0.0 , '2020-06-07T02:44:05.392777', 'tk3', 'c',  201.0 ,  307.53000000000003 , 'fl1', 'hm2')");
			stat.executeUpdate("INSERT INTO ticket VALUES( '2020-02-02T02:03:02', 'Toronto', '2020-02-02T02:02:02',  200.0 ,  306.0 ,  0.0 ,  201.0 ,  0.0 , '2020-06-07T02:44:05.392777', 'tk4', 'b',  201.0 ,  307.53000000000003 , 'fl1', 'hm2')");
			stat.executeUpdate("INSERT INTO ticket VALUES( '2020-02-02T02:03:02', 'Toronto', '2020-02-02T02:02:02',  200.0 ,  306.0 ,  0.0 ,  201.0 ,  0.0 , '2020-06-07T02:44:05.392777', 'tk5', 'b',  201.0 ,  307.53000000000003 , 'fl1', 'hm2')");

			/*
			 * Main loop
			 */

			int inp         = 0;
			int exitTrigger = 1;
			while ( exitTrigger == 1 ) {
System.out.println(
"                                            _                   \n" +
"   __,   .  ,_  ,_    _,_ ,_  -/-     __   //  .  _   ,__,  -/- \n" +
"  (_/(__/__/ (__/__ +(_/_/ (__/_    _(_,__(/__/__(/__/ / (__/_  \n" +
"               /                                                \n" +
"              /                                                 \n" +
"                                                                \n" +
"     1. To insert new flight                                    \n" +
"     2. To insert new customer                                  \n" +
"     3. To insert new ticket                                    \n" +
"     4. To return flights                                       \n" +
"     5. To return full flights                                  \n" +
"     6. Return flights from Toronto to New york                 \n" +
"     7. Delete canceled tickets                                 \n" +
"     8. Payment portal                                          \n" +
"                                                                \n" +
"     0. Exit                                                    \n" +
"                                                                \n");
				Scanner scan = new Scanner(System.in);
				inp = isMenuOption();

				/*
				 * Selection
				 */

				switch (inp){
					case 0: // Opt 0 Exit
						clearScreen();
						exitTrigger = 0;
						break;
					case 1: // Opt1 new Flight
						try{
							Flight fl = new Flight(stat);
							stat.executeUpdate(fl.returnDbData());
						}catch(Exception e){
							System.out.println("ERROR : Flight generation \n {"+
									e.getMessage()+ "}\n");//<-
						}
						clearScreen();
						break;
					case 2: // Opt2 New customer
						try{
							Customer cs = new Customer();
							stat.executeUpdate(cs.returnDbData());
						}catch(Exception e){
							System.out.println("ERROR : Customer generation \n {"+
									e.getMessage()+ "}\n");//<-
						}
						clearScreen();
						break;
					case 3: // Opt3 New Ticket
						try{
							Ticket tk = new Ticket(stat);
							stat.executeUpdate(tk.returnDbData());
						}catch(Exception e){
							System.out.println("ERROR : Ticket generation \n {"+
									e.getMessage()+ "}\n");//<-
						}
						clearScreen();
						break;
					case 4: // Opt4 return flights
						Flight.checkFlights(stat);
						break;
					case 5: // Opt5 return full flights
						Flight.checkFullFlights(stat);
						break;
					case 6: // Opt6
						try{
							ResultSet rs = stat.executeQuery("SELECT * FROM `flight` WHERE `departureCity`" +
							" = 'Toronto' AND `arrivalCity` = 'New york' ORDER BY `flight`.`flightId` ASC");
							while(rs.next())
								System.out.println("Flights -> " + rs.getString("flightId") + "\n" );//<-
						}catch(Exception e){
							System.out.println("exception");//<-
						}
						break;
					case 7: // Opt7
						clearScreen();
						PaymentPortal.deleteCanceledTickets(stat);
						break;
					case 8: // Opt8
						clearScreen();
						PaymentPortal.main();
						clearScreen();
						break;
				} 	// End of switch
			} 	// End of while

		}catch(Exception e){
			System.out.println("ERROR: Client.main" +
					e.getMessage() );

		} 	// End of Trycatch
	}	// End of main


	/*
	 * Basic functions
	 */


	public static void clearScreen() {
		for (int i = 0; i < 50; ++i) { //clear console simulation for Eclipse
			System.out.println();
		}
		System.out.print("\033\143\n"); //for terminal
	}


	public static void pressEnter(){
		Scanner sc = new Scanner(System.in);
		System.out.println("Press Enter to continue...");
		sc.nextLine();
	}


	public static int isMenuOption() {
		Scanner sc = new Scanner(System.in);
		int number;
		do {
			System.out.print("[ console ~] : ");
			while (!sc.hasNextInt()) {
				System.out.println(">Not an integer");
				System.out.print("[ console ~] : ");
				sc.next();
			}
			number = sc.nextInt();
		} while (number < 0 || number > 8);
		return number;
	}


	/*
	 * Input functions
	 */


	public static String selectFromCountry(Statement stat){
		ArrayList<String> Countries = new ArrayList<>();
		try{
			ResultSet rs = stat.executeQuery("SELECT * FROM `country`");
			while (rs.next())
				Countries.add(rs.getString("countryName"));
		}catch (Exception e){
			System.err.println("exception in selectFromCountry\n" +
					e.getMessage() + "\n");
		}
		String input;
		do{
			System.out.println("\n" + Countries + "\n");
			input = Client.askString("Select country from above (Enter exit to quit)> ");
			if(input.equals("exit")){
				return null;
			}
		}while (!Countries.contains(input));
		return input;
	}


	public static String askString(String message){
		Scanner sc = new Scanner(System.in);
		System.out.print(message);
		String string;
		string = sc.nextLine();
		System.out.println("{" + string + "} Inserted");
		return string;
	}


	public static int askInteger(String message) {
		Scanner sc = new Scanner(System.in);
		System.out.print(message);
		int number;
		while (!sc.hasNextInt()) {
			System.out.println(">Not an integer");
			System.out.print(message);
			sc.next();
		}
		number = sc.nextInt();
		return number;
	}


	public static boolean askBoolean(String message){
		Scanner sc = new Scanner(System.in);
		System.out.print(message);
		boolean bool;
		while (!sc.hasNextBoolean()) {
			System.out.println(">Not a Boolean value");
			System.out.print(message);
			sc.next();
		}
		bool = sc.nextBoolean();
		return bool;
	}


	public static double askDouble(String message){
		Scanner sc = new Scanner(System.in);
		System.out.print(message);
		double doub;
		while (!sc.hasNextDouble()) {
			System.out.println(">Not a Double value");
			System.out.print(message);
			sc.next();
		}
		doub = sc.nextDouble();
		return doub;
	}


	public static LocalDateTime askDateTime(String message){
		System.out.println(message + "\n");//<-
		int year, month, day, hour, min, sec;
		do {
			year = askInteger("\n>Enter date (year) : ");
		} while (year < 1880 || year > 2090 );
		do {
			month = askInteger("\n>Enter date (month) : ");
		} while (month < 1 || month > 12 );
		do {
			day = askInteger("\n>Enter date (day) : ");
		} while (day < 1 || day > 31);
		do {
			hour = askInteger("\n>Enter time (hour) : ");
		} while (hour < 0 || hour > 23);
		do {
			min = askInteger("\n>Enter time (minutes) : ");
		} while (min < 0 || min > 59);
		do {
			sec = askInteger("\n>Enter time (sec) : ");
		} while (sec < 0 || sec > 59);
		LocalDateTime datetime = LocalDateTime.of(year, month, day, hour, min, sec);
		System.out.println("{" + datetime + "} Inserted");
		return datetime;
	}   // End of checkFlightDate


}	// End of class
