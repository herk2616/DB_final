import java.sql.*;
import java.util.Scanner;
import java.util.ArrayList;
import java.time.LocalDateTime;


class Ticket{
	private Statement stat;
	private String ArrivalDateTime;
	private String CityName;
	private String DepartureDateTime;
	private double FlightPrice;
	private double FlightPriceLocal;
	private double PriceDeposited;
	private double PriceRemaining;
	private double TicketBussinessFlag;
	private LocalDateTime TicketDate;
	private String TicketId;
	private String TicketStatus;
	private double TotalPrice;
	private double TotalPriceLocal;
	private String Flight_flightId;
	private String Customer_customerId;
	private double Tax;
	private double Exrate;


	public Ticket(Statement stat){
		TicketBussinessFlag = Client.askInteger("Is the seat in bussines class? [1/0] > ");
		if( TicketBussinessFlag == 1){
			Flight_flightId = Flight.selectFromAvBussinesFlights(stat);
			decreaseFlightBussinessSeat(stat, Flight_flightId);
		}else{
			Flight_flightId = Flight.selectFromAvEconomyFlights(stat);
			decreaseFlightEconomySeat(stat, Flight_flightId);
		}
		Customer_customerId = Customer.selectFromCustomer(stat);
		CityName = Flight.selectFromBooking(stat);
		TicketId = Client.askString("Enter ticket id > ");
		TicketStatus = "c";
		// Time of ticket creation
		TicketDate = LocalDateTime.now();
		PriceDeposited = 0;
		try {
			ResultSet rs = stat.executeQuery("SELECT * FROM `flight` WHERE flightId = '" + Flight_flightId + "'");
			while(rs.next()){
				// Ticket price
				FlightPrice = rs.getDouble("ticketPrice");
				// Time and date
				DepartureDateTime = rs.getString("departureDateTime");
				ArrivalDateTime =  rs.getString("arrivalDateTime");
			}
		}catch(Exception e){
			System.out.println("Error in ticket constractor flight selection" + e.getMessage());
		}
		//PROBLEM: Coustomer can be from anywere, cannot find his currency
		//COPE: I will use currency of the country were the ticket was initialised
		String airline = Flight.fromCitytoAirline(stat, CityName);
		String country = Flight.fromAirlinetoCountry(stat, airline);
		Tax = fromCountrytoTax(stat, country);
		Exrate = fromCountrytoExchangeRate(stat, country);
		TotalPrice = FlightPrice + Tax;
		TotalPriceLocal = TotalPrice * Exrate;
		FlightPriceLocal = FlightPrice * Exrate;
		PriceRemaining = TotalPrice - PriceDeposited;
	}


	public String returnDbData(){
		return "INSERT INTO ticket VALUES( " +
			"'" + this.ArrivalDateTime + "', " +
			"'" + this.CityName + "', " +
			"'" + this.DepartureDateTime + "', " +
			" " + this.FlightPrice + " , " +
			" " + this.FlightPriceLocal + " , " +
			" " + this.PriceDeposited + " , " +
			" " + this.PriceRemaining + " , " +
			" " + this.TicketBussinessFlag + " , " +
			"'" + this.TicketDate + "', " +
			"'" + this.TicketId + "', " +
			"'" + this.TicketStatus + "', " +
			" " + this.TotalPrice + " , " +
			" " + this.TotalPriceLocal + " , " +
			"'" + this.Flight_flightId + "', " +
			"'" + this.Customer_customerId + "')";
	}


	public Double fromCountrytoExchangeRate(Statement stat, String country){
		double Exrate = 0.0;
		try{
			String Q = "SELECT `exchangeRate` FROM `country` WHERE `countryName` = '";
			ResultSet rs = stat.executeQuery(Q + country + "'");
			while (rs.next())
				Exrate = rs.getDouble("exchangeRate");
		}catch (Exception aa){
			System.err.println("exception in fromCountrytoExchangeRate \n" +
					aa.getMessage() + "\n");
		}
		return Exrate;
	}


	public Double fromCountrytoTax(Statement stat, String country){
		double Tax = 0.0;
		try{
			String Q = "SELECT `tax` FROM `country` WHERE `countryName` = '";
			ResultSet rs = stat.executeQuery(Q + country + "'");
			while (rs.next())
				Tax = rs.getDouble("tax");
		}catch (Exception aa){
			System.err.println("exception in fromCountrytoExchangeRate \n" +
					aa.getMessage() + "\n");
		}
		return Tax;
	}


	public void decreaseFlightEconomySeat(Statement stat, String flight){
		try{
			stat.executeUpdate("UPDATE flight SET economyReserved = economyReserved -1 WHERE flightId = '" + flight + "'");
			ResultSet rs = stat.executeQuery("SELECT * FROM `flight` WHERE `flightId` = '" + flight +"'");
			int res = 0;
			int total = 0;
			while(rs.next()){
				res = rs.getInt("economyReserved");
				total = rs.getInt("economySeats");
			}
			if (res >= total){
				stat.executeUpdate("UPDATE `flight` SET `economyFlag` = 1 WHERE `flightId` = '"+ flight +"'");
			}
		}catch(Exception e){
			System.out.println("Exception in decreaseFlightEconomySeat");
		}
	}


	public void decreaseFlightBussinessSeat(Statement stat, String flight){
		try{
			stat.executeUpdate("UPDATE flight SET bussinessReserved = bussinessReserved -1 WHERE flightId = '" + flight + "'");
			ResultSet rs = stat.executeQuery("SELECT * FROM `flight` WHERE `flightId` = '" + flight +"'");
			int res = 0;
			int total = 0;
			while(rs.next()){
				res = rs.getInt("bussinessReserved");
				total = rs.getInt("bussinessTotal");
			}
			if (res >= total){
				stat.executeUpdate("UPDATE `flight` SET `bussinessFlag` = 1 WHERE `flightId` = '"+ flight +"'");
			}
		}catch(Exception e){
			System.out.println("Exception in decreaseFlightbussinessSeat");
		}
	}


}	// End of class
